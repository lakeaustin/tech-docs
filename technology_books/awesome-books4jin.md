# Awesome Books for Jin


## Machine Learning and DL





## Python

| Book | Path | 说明 |
|:------------| :------------|:-------------|
| the-django-book-2 | the-django-book-2 | 适合基础入门，把django每个部分都讲解了一遍 |




## Others


| Book | Path | 说明 |
|:------------| :------------|:-------------|
| Kubernetes_in_Action | [k8s/Kubernetes_in_Action.pdf] | 介绍清晰易懂，适合入门；图列很清楚地解释了相关概念呢 |
