# django docs and books



[django文档](https://docs.djangoproject.com)

[How To Tango With Django 1.7](https://www.tangowithdjango.com/book17)

https://github.com/leifos/tango_with_django_2




《the django book》第二版

我相信这是很多人学习django看的第一本书，我也是。很好的一本入门书，菜鸟也能看懂的。缺点;书太老了，应该更新了，django是基于django1.0的，现在是django1.9了，改变了很多，如果按书上的，你可能会遇到很多麻烦的，同时也是进步的过程。




Django应用、项目和资源集合：Awesome Django（强烈推荐）

https://github.com/wsvincent/awesome-django



https://www.tangowithdjango.com




The Django Book 2.0中文版 https://www.kancloud.cn/thinkphp/django-book/39581
